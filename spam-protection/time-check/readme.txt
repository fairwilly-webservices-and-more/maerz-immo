**************************************************
*** Formhandler - SPAM Protection - Time Check ***
**************************************************

This example will show a simple contact form SPAM protected using a time based check.
Filling out the form must take longer than a specified amount of time. 
If the form gets submitted before this threshold time, the request is treated as SPAM. 
After the user submitted the form, an email gets sent to an administrator.

* Include the TypoScript in the folder "ts".
* Configure the path to the files and other settings using the TypoScript Constant Editor.

**********************
****** Example *******
**********************

<INCLUDE_TYPOSCRIPT: source="FILE: fileadmin/formhandler/spam-protection/time-check/ts/ts_setup.txt">
